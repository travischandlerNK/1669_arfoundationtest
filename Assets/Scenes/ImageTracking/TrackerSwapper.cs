﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.XR;

public class ImageTargetObject
{
    public float timer = 0f;
    public GameObject trackedObject;

    public ImageTargetObject(GameObject game)
    {
        timer = 0f;
        trackedObject = game;
    }
}
public class TrackerSwapper : MonoBehaviour
{

    public ARTrackedImageManager imageManager;
    public PlaceOnPlane planePlacer;
    public ARPlaneManager planeManager;
    public Dictionary<string, ImageTargetObject> trackableIDObjectPairs;
    public GameObject targetPrefabIfPoe;

    // Start is called before the first frame update
    void Start()
    {
        trackableIDObjectPairs = new Dictionary<string, ImageTargetObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if (imageManager.enabled)
        {
            foreach (ARTrackedImage tracked in imageManager.trackables)
            {
                ImageTargetObject obj;
                if(trackableIDObjectPairs.TryGetValue(tracked.referenceImage.name, out obj))
                {
                    if(tracked.trackingState == TrackingState.Limited)
                    {
                        obj.timer += Time.deltaTime;
                        if(obj.timer >= limitedTrackingTime)
                        {
                            obj.trackedObject.SetActive(false);
                        }
                    }
                }
            }
        }
    }

    public void OnEnable()
    {
        imageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    public void OnDisable()
    {
        imageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }

    public void SwapTrackers()
    {
        imageManager.enabled = !imageManager.enabled;
        planeManager.enabled = !planeManager.enabled;
        planePlacer.enabled = planeManager.enabled;

        if(!planePlacer.enabled)
        {
            planePlacer.DestroySpawnedObject();
        }
        SetAllPlanesActive(planeManager.enabled);
        SetAllTrackedImagesActive(imageManager.enabled);
    }



    public void AddNewTrackedImage(ARTrackedImage trackedImage)
    {
        // Disable the visual plane if it is not being tracked
        if (trackedImage.trackingState != TrackingState.None)
        {
            // The image extents is only valid when the image is being tracked
            trackedImage.transform.localScale = new Vector3(trackedImage.size.x, 1f, trackedImage.size.y);

            if (trackedImage.referenceImage.name.Equals("poe_sketchPortrait_LCE"))
            {
                GameObject game = Instantiate(targetPrefabIfPoe, trackedImage.transform);
                game.transform.localScale *= 0.5f;
                trackableIDObjectPairs.Add(trackedImage.referenceImage.name, new ImageTargetObject(game));
            }
        }
    }

    public float limitedTrackingTime = 1f;
    public void UpdateTrackedImage(ARTrackedImage trackedImage)
    {
        if (trackedImage.referenceImage.name.Equals("poe_sketchPortrait_LCE"))
        {
            if (trackedImage.trackingState == TrackingState.Tracking)
            {

                ImageTargetObject targetObject = trackableIDObjectPairs[trackedImage.referenceImage.name];
                targetObject.timer = 0f;
                targetObject.trackedObject.SetActive(true);
                // The image extents is only valid when the image is being tracked
                trackedImage.transform.localScale = new Vector3(trackedImage.size.x, 1f, trackedImage.size.y);
            }
            else if (trackedImage.trackingState == TrackingState.Limited)
            {
                ImageTargetObject targetObject = trackableIDObjectPairs[trackedImage.referenceImage.name];
                targetObject.timer += Time.deltaTime;
                if (targetObject.timer > limitedTrackingTime)
                {
                    targetObject.trackedObject.SetActive(false);
                }
            }
            else
            {

            }
        }

    }

    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (var trackedImage in eventArgs.added)
        {
            // Give the initial image a reasonable default scale
            trackedImage.transform.localScale = new Vector3(0.01f, 1f, 0.01f);
            AddNewTrackedImage(trackedImage);
        }

        foreach (var trackedImage in eventArgs.updated)
        {
            UpdateTrackedImage(trackedImage);
        }
    }

    public void SetAllTrackedImagesActive(bool active)
    {
        foreach(ARTrackedImage tracked in imageManager.trackables)
        {
            ImageTargetObject targetObject = trackableIDObjectPairs[tracked.referenceImage.name];
            targetObject.timer = 0f;
            tracked.gameObject.SetActive(active);
            targetObject.trackedObject.SetActive(false);
        }
    }

    public void SetAllPlanesActive(bool active)
    {
        foreach(ARPlane plane in planeManager.trackables)
        {
            plane.gameObject.SetActive(active);
        }
    }
}
