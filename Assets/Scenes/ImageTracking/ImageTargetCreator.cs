﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using UnityEngine.Jobs;
using UnityEngine.XR;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Unity.Jobs;
using System;
public class ImageTargetCreator : MonoBehaviour
{
    public ARTrackedImageManager imageManager;
    public XRReferenceImageLibrary imageLibrary;
    RuntimeReferenceImageLibrary runtimeLibrary;
    JobHandle job;
    List<RuntimeReferenceImageLibrary> runtimeLibraries;
    public Texture2D tex;
    bool jobCompletedAlready = false;
    // Start is called before the first frame update
    void Start()
    {
        if(imageManager.referenceLibrary is MutableRuntimeReferenceImageLibrary library)
        {

            NativeSlice<byte> bytes = tex.GetRawTextureData<byte>();
            SerializableGuid guid = new SerializableGuid();
            XRReferenceImage refImage = new XRReferenceImage(guid, guid, new Vector2(1, 1), "testTarget", tex);
            job = library.ScheduleAddImageJob(tex, "test", 1f);
            runtimeLibraries = new List<RuntimeReferenceImageLibrary>();
            DisableAllImages();

        }
        else
        {
            Debug.Log("Not a mutable library");
        }


    }


    public void DisableAllImages()
    {
        if (imageManager.referenceLibrary is MutableRuntimeReferenceImageLibrary library)
        {
            for(int x = 0; x < library.count; x++)
            {
                runtimeLibraries.Add(imageManager.CreateRuntimeLibrary());
                (runtimeLibraries[x] as MutableRuntimeReferenceImageLibrary).ScheduleAddImageJob(library[x].texture, library[x].name, library[x].width);
            }
        }
        imageManager.referenceLibrary = imageManager.CreateRuntimeLibrary();

    }

    // Update is called once per frame
    void Update()
    {
        if(job.IsCompleted)
        {
            if(!jobCompletedAlready)
            {

                jobCompletedAlready = true;
            }
        }
    }

    public void SetImageTargetLibrary(int x)
    {
        imageManager.referenceLibrary = runtimeLibraries[x];
        imageManager.subsystem.imageLibrary = runtimeLibraries[x];
        imageManager.subsystem.Start();
        Debug.Log("Enabled first trackable");
    }
}
